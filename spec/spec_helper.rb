# This file was generated by the `rspec --init` command. Conventionally, all
# specs live under a `spec` directory, which RSpec adds to the `$LOAD_PATH`.
# Require this file using `require "spec_helper"` to ensure that it is only
# loaded once.
#
# See http://rubydoc.info/gems/rspec-core/RSpec/Core/Configuration
require 'publishable'
RSpec.configure do |config|
  config.treat_symbols_as_metadata_keys_with_true_values = true
  config.run_all_when_everything_filtered = true
  config.filter_run :focus

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = 'random'
end

def setup
   ActiveRecord::Base.establish_connection(
    :adapter => 'sqlite3',
    :database => ':memory:'
  )

  ActiveRecord::Schema.define do
    create_table :test_default_publishables, force: true do |t|
      t.column :name, :string
      t.column :text, :string
      t.column :published, :bool
      t.column :dirty, :bool
    end

    create_table :test_custom_publishables, force: true do |t|
      t.column :title, :string
      t.column :text, :string
      t.column :published, :bool
      t.column :dirty, :bool
    end
  end
end

class TestDefaultPublishable < ActiveRecord::Base
  publishable
end

class TestCustomPublishable < ActiveRecord::Base
  publishable name: :title
end
