# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'publishable/version'

Gem::Specification.new do |spec|
  spec.name          = "publishable"
  spec.version       = Publishable::VERSION
  spec.authors       = ["Gilad Zohari"]
  spec.email         = ["gzohari@gmail.com"]
  spec.description   = %q{bar}
  spec.summary       = %q{foo}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency 'activerecord', '>= 3.0'
  spec.add_runtime_dependency 'activesupport', '>= 3.0'
  spec.add_runtime_dependency 'protected_attributes'
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "spec"
  spec.add_development_dependency 'sqlite3'
end
