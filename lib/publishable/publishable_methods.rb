require 'active_support'
module PublishableMethods
  extend ActiveSupport::Concern
  included do
    class_attribute :publishable_column_name
    attr_accessor :publishable_column_name

    attr_accessible :published, :dirty
    scope :published, lambda { where published: true }
    scope :drafts,    lambda { where("published IS NULL or  published = 0") }
    scope :dirty,     lambda { where dirty: true }

    before_save :set_dirty, unless: 'published? || dirty_changed?'
    after_destroy :destroy_published
  end

  def publishable_identifier
    self.class.publishable_column_name
  end

  def published_version(create_published: true)
    return self if published?
    version = self.class.published.where("#{publishable_identifier}" => self.name).first
    version || create_published_version  

  end

  def create_published_version
    published_version = self.dup
    published_version.update_attribute(:published, true)
    published_version
  end

  def copyable_atts
    self.attributes.reject {|k,v| %w( name draft id created_at updated_at ).include? k }
  end

  def publish
    self.copyable_atts.each { |pair| published_version.update_attribute(*pair) }
    write_attribute :dirty, false
  end

  ## creates new record when discarding!
  def discard
    self.update_attributes(published_version.copyable_atts.merge(dirty: false))
  end

  private

  def set_dirty
    self.dirty = true
  end

  def destroy_published
    return if published?
    published_version(create_published: false).destroy
  end
end
