require "spec_helper"

describe Publishable do
    before do 
      setup
    end

  # publishable, column_name: :title
  context "default publishable column name" do
    context "new record" do
      before do 
        TestDefaultPublishable.create!(name: "A donkey, a donkey, my kingdom for a donkey", text: "I see your HEV Suit still fits you like a glove.")
      end
        it "is a draft by default" do
          expect(TestDefaultPublishable.drafts.count).to eq 1
        end
        
        it "isn't published" do
          expect(TestDefaultPublishable.published.count).to eq 0
        end
    end
    
    context "existing record" do
      before do 
        draft = TestDefaultPublishable.create!(name: "A donkey, a donkey, my kingdom for a donkey", text: "I see your HEV Suit still fits you like a glove.")
        draft.publish
      end
      describe "publish" do
        it "creates an identical published version" do
          expect(TestDefaultPublishable.published.first.attributes).to eq TestDefaultPublishable.attributes
        end
        it "publishes the draft" do
          expect(TestDefaultPublishable.published.count).to eq 1
        end
        it "it keeps the draft" do
          expect(TestDefaultPublishable.drafts.length).to eq 1
        end
      end

      describe "save draft" do
      before do 
        TestDefaultPublishable.create!(name: "A donkey, a donkey, my kingdom for a donkey", text: "I see your HEV Suit still fits you like a glove.")
      end
        let(:draft) { (TestDefaultPublishable).drafts.first }
        it "doesn't publish" do
          draft.should_not_receive(:publish)
        end
        it "doesn't destroy published version" do
          draft.publish
          draft.reload
          draft.save
          expect(TestDefaultPublishable.published.count).to eq 1
        end
      end

      describe "destroy" do
        it "destroyes published version"
      end
    end

      
  end
  context 'custom publishable name' do
    it "find_drafts_by_custom name"
  end
end
