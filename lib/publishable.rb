require "publishable/version"
require "publishable/publishable_methods"
require 'protected_attributes'
require 'active_record'

#It relies on 'name' column to identify the published entity, and 'dirty' and 'published' boolean columns that should be added to the model.

class ActiveRecord::Base
  extend Publishable
end

module Publishable
  def publishable(name: 'name')
    include PublishableMethods
    self.publishable_column_name = name
  end
end

